# Student Management



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:4c5bb723c2b752b4750efeb22173a33c?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:4c5bb723c2b752b4750efeb22173a33c?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:4c5bb723c2b752b4750efeb22173a33c?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/ajithmkd.m/student-management.git
git branch -M main
git push -uf origin main
```

## Runnig the Project Localy

- Initially clone the project and enter in to the project directory student_management and then install the requirement.txt file inside python environment.
- Use the python environment with version above 3.6.
- Once installation gets completed using the command line python manage.py migrate migrate the database.
- After the database migration run python manage.py runserver for starting the server.
- Attaching the postman collection along with the project as a link https://www.postman.com/collections/8c78ea58ebeab5e97a20.
- For impoting the collection in Aostman App, click the import button on the left top of the App and choose link section and attach the collection link there and continue. 
